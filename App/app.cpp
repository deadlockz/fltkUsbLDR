#include "gui.h"
#include <string>
#include <sstream>
using namespace std;

void cb(Fl_Widget * ob) {
	string in = input->value();
	if (in.back() == ' ') {
		int result;
		stringstream(in) >> result;
		dial->value(result);
		input->value("");
	}
}

int main() {
	Fl_Double_Window * win = make_window();
	
	input->when(FL_WHEN_CHANGED);
	input->callback(cb);
	
	win->show();
	return Fl::run();
}
