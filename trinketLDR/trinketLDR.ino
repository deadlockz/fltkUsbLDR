#include <TrinketKeyboard.h>

#define PIN_BUTTON   0
#define PIN_LED      1

int  PIN_LDR = 1; // ANALOG_1 = #2 (PB2)
int  poti;
bool isRunning = false;

void setup() {
  pinMode(PIN_BUTTON, INPUT);
  pinMode(PIN_LED, OUTPUT);
  digitalWrite(PIN_BUTTON, HIGH);
  TrinketKeyboard.begin();
}

void loop() {
  TrinketKeyboard.poll();
  poti = analogRead(PIN_LDR);
  if (digitalRead(PIN_BUTTON) == LOW) {
    delay(500);
    if (digitalRead(PIN_BUTTON) == LOW) {
      if (isRunning) {
        isRunning = false;
        digitalWrite(PIN_LED, LOW);
      } else {
        isRunning = true;
        digitalWrite(PIN_LED, HIGH);
      }
    }
  }
  if (isRunning) {
    TrinketKeyboard.print(poti);
    TrinketKeyboard.print(" ");
  }
}
